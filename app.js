const express = require('express')
const path = require('path')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const expressValidator = require('express-validator')
const session = require('express-session')
const flash = require('connect-flash')
const passport = require('passport')


mongoose.connect('mongodb://localhost/bookshelf_db')
let db = mongoose.connection

//check db connection
db.once('open', function () {
    console.log('Connected to MongoDb')
});

//check db errors
db.on('error', function (err) {
    console.log(err)
})


//Initiate app
const app = express()

//initiate model
let Book = require('./models/book')
//routes
let books = require('./routes/books')
let users = require('./routes/users')


//view engine
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')
//body parser middleware
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())


//set public folder
app.use(express.static(path.join(__dirname, 'public')))
//session
app.use(session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
}))
//express messages
app.use(flash());

app.use(function (req, res, next) {
    res.locals.messages = require('express-messages')(req, res);
    next();
});
//express validator
app.use(expressValidator({
    errorFormatter: function (param, msg, value) {
        var namespace = param.split(','), root = namespace.shift(), formParam = root;

        while (namespace.length) {
            formParam += '[' + namespace.shift() + ']';
        }
        return {
            param: formParam,
            msg: msg,
            value: value
        }
    }
}))

require('./config/passport')(passport)

app.use(passport.initialize())
app.use(passport.session())
app.use('/books', books)
app.use('/users', users)


app.get('*', function (req, res, next) {
    res.locals.user = req.user || null;
    next()
})


//index page
app.get('/', function (req, res) {

    Book.find({}, function (err, books) {

        if (err) {
            console.log(err)
        }
        else {
            res.render('books/index', {
                title: 'Bookshelf',
                books_array: books,
                errors: err,
                user: req.user
            });
        }
    });

});


app.post('/', function (req, res) {

    const regex_genre = new RegExp(escapeRegex(req.body.genre), 'gi');
    const regex_title = new RegExp(escapeRegex(req.body.title), 'gi');

    if (req.body.title) {
        Book.find({title: regex_title}, function (err, books) {

            if (err) {
                console.log(err)
                return
            }
            else {
                // return res.status(200).json(
                //     {
                //         text_status: 'success',
                //         books
                //     }
                //
                // )


                return res.render('books/index', {
                    title: 'Bookshelf',
                    books_array: books,
                    errors: req.errors,
                    user: req.user
                });


            }

        });

    } else if (req.body.genre) {
        Book.find({genre: regex_genre}, function (err, books) {

            if (err) {
                console.log(err)
                return
            }
            else {
                // return res.status(200).json(
                //     {
                //         text_status: 'success',
                //         books
                //     }
                //
                // )


                return res.render('books/index', {
                    title: 'Bookshelf',
                    books_array: books,
                    errors: req.errors,
                    user: req.user
                });


            }

        });
    }else {
        Book.find({}, function (err, books) {

            if (err) {
                console.log(err)
                return
            }
            else {
                // return res.status(200).json(
                //     {
                //         text_status: 'success',
                //         books
                //     }
                //
                // )


                return res.render('books/index', {
                    title: 'Bookshelf',
                    books_array: books,
                    errors: req.errors,
                    user: req.user
                });


            }

        });
    }
});

function escapeRegex(text) {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
};


app.listen(3000, function (req, res) {
    console.log('Server is running smoothly')
})