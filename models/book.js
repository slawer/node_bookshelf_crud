let mongoose = require('mongoose')

let bookSchema = mongoose.Schema({
    title:{
        type: String,
        required: true
    },
    author:{
        type: String,
        required: true
    },
    genre:{
        type: String,
        required: true
    }
});

const Book = module.exports = mongoose.model('Books',bookSchema)