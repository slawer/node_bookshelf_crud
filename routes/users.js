const express = require('express')
const router = express.Router()
const bcrypt = require('bcryptjs')
const passport = require('passport')


//initiate model
let User = require('../models/user')

//registration
router.get('/register', function (req, res) {
    res.render('users/register', {
        errors: req.errors,
        user: req.user
    })
})

router.post('/register', function (req, res) {

    const name = req.body.name
    const email = req.body.email
    const username = req.body.username
    const password = req.body.password
    const confirm_password = req.body.confirm_password

    req.checkBody('name', 'Name is required').notEmpty()
    req.checkBody('email', 'Email is required').notEmpty()
    req.checkBody('username', 'Username is required').notEmpty()
    req.checkBody('password', 'Password is required').notEmpty()
    req.checkBody('name', 'Name is required').notEmpty()
    req.checkBody('confirm_password', 'Confirm Password is required').equals(req.body.password)

    let errors = req.validationErrors()

    if (errors) {
        res.render('users/register', {
            errors: errors,
            user: req.user
        })
    } else {
        let newUser = new User({
            name: name,
            email: email,
            username: username,
            password: password
        })

        bcrypt.genSalt(10, function (err, salt) {
            bcrypt.hash(newUser.password, salt, function (err, hash) {
                if (err) {
                    console.log(err)
                }
                newUser.password = hash
                newUser.save(function (err) {
                    if (err) {
                        console.log(err)
                        return
                    } else {
                        req.flash('success', 'Registration successful, kindly login')
                        res.redirect('/users/login')
                    }

                })
            })
        })
    }
})

//login
router.get('/login', function (req, res) {
    res.render('users/login', {
        errors: req.errors,
        user: req.user
    })
})

router.post('/login', function (req, res, next) {
    passport.authenticate('local', {
        successRedirect: '/',
        failureRedirect: '/users/login',
        failureFlash: true
    })(req, res, next)
})

router.get('/logout', function (req, res) {

    req.logout()
    req.flash('success','You are logged out')
    res.redirect('/')
})

module.exports = router