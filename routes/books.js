const express = require('express')
const router = express.Router()


//initiate model
let Book = require('../models/book')



//Edit page
router.post('/edit/:id',checkAuth, function (req, res) {
    let book = {};

    console.log(req.body.author)

    book.title = req.body.title
    book.author = req.body.author
    book.genre = req.body.genre

    let query = {_id: req.params.id}

    Book.update(query, book, function (err) {
        if (err) {
            console.log(err)
            return
        } else {
            req.flash('success', 'Book updated successfully')
            res.redirect('/')
        }
    })
    console.log('Updated')
    return
});

router.get('/edit/:id',checkAuth, function (req, res) {
    let book_id = req.params.id


    Book.findById(book_id, function (err, book) {
        if (err) {
            console.log(err)
        } else {
            res.render('books/edit_book', {
                book: book,
                title: 'Edit Book',
                errors: req.errors,
                user: req.user
            })
        }
    })
    console.log('submited')
    return
});


//new book page
router.get('/new', checkAuth,function (req, res) {

    res.render('books/new_book', {
        title: 'New Book',
        errors: req.errors,
        user: req.user
    });
});

router.post('/add',checkAuth, function (req, res) {
    req.checkBody('title', 'Title is required').notEmpty()
    req.checkBody('author', 'Author is required').notEmpty()
    req.checkBody('genre', 'Genre is required').notEmpty()

    const norm = req.validationErrors()
    let errors = req.validationErrors()
    console.log(errors)
    if (errors) {
        res.render('books/new_book', {
            title: 'New Book',
            errors: errors,
            user: req.user

        })

       // req.flash('danger', 'Book added successfully')
    } else {
        let book = new Book()

        book.title = req.body.title
        book.author = req.body.author
        book.genre = req.body.genre


        book.save(function (err) {
            if (err) {
                console.log(err)
                return
            } else {
                req.flash('success', 'Book added successfully')
                res.redirect('/')
            }
        })
    }
});


//delete page
router.get('/delete/:id',checkAuth, function (req, res) {
    let query = {_id: req.params.id}

    Book.findByIdAndDelete(query, function (err) {
        if (err) {
            console.log(err)
        } else {

            req.flash('danger', 'Book deleted successfully')
            res.redirect('/')
        }

    })
})

//show page
router.get('/:id',checkAuth, function (req, res) {
    let book_id = req.params.id


    Book.findById(book_id, function (err, book) {
        if (err) {
            console.log(err)
            return
        } else {
            res.render('books/show_book', {
                book: book,
                title: 'Bookshelf',
                errors: [],
                user: {}
            })
        }
    })
    console.log('submited')
    return
});




//access control
function checkAuth(req, res, next) {
    if (req.isAuthenticated()) {
        return next()
    } else {
        req.flash('danger', 'Please login to continue')
        res.redirect('/')
    }
}



module.exports = router
